import { defineConfig } from "cypress";
import * as Webpack from "webpack";
import wp from "@cypress/webpack-preprocessor";
import { devServer } from "@cypress/webpack-dev-server";
import { addCucumberPreprocessorPlugin } from "@badeball/cypress-cucumber-preprocessor";

const webpackConfig = (
    cypressConfig: Cypress.PluginConfigOptions
): Webpack.Configuration => {
    return {
        resolve: {
            extensions: [".js", ".ts", ".tsx"],
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: [/node_modules/],
                    use: [
                        {
                            loader: "ts-loader",
                            options: { transpileOnly: true },
                        },
                    ],
                },
                {
                    test: /\.feature$/,
                    use: [
                        {
                            loader: "@badeball/cypress-cucumber-preprocessor/webpack",
                            options: cypressConfig,
                        },
                    ],
                },
            ],
        },
    };
};

export default defineConfig({
    component: {
        specPattern: "cypress/component/*.feature",
        supportFile: false,
        devServer(devServerConfig) {
            return devServer({
                ...devServerConfig,
                framework: "react",
                webpackConfig: webpackConfig(devServerConfig.cypressConfig),
            });
        },
        async setupNodeEvents(on, config) {
            // This is required for the preprocessor to be able to generate JSON reports after each run, and more.
            await addCucumberPreprocessorPlugin(on, config);

            // Make sure to return the config object as it might have been modified by the plugin.
            return config;
        },
    },
    e2e: {
        specPattern: "cypress/e2e/*.feature",
        supportFile: false,
        /* devServer(devServerConfig) {
            return devServer({
                ...devServerConfig,
                framework: "react",
                webpackConfig: webpackConfig(devServerConfig.cypressConfig),
            });
        }, */
        async setupNodeEvents(on, config) {
            await addCucumberPreprocessorPlugin(on, config);
            
            on(
                "file:preprocessor",
                wp({
                    webpackOptions: {
                        resolve: {
                            extensions: [".ts", ".js"],
                        },
                        module: {
                            rules: [
                                {
                                    test: /\.ts$/,
                                    exclude: [/node_modules/],
                                    use: [
                                        {
                                            loader: "ts-loader",
                                        },
                                    ],
                                },
                                {
                                    test: /\.feature$/,
                                    use: [
                                        {
                                            loader: "@badeball/cypress-cucumber-preprocessor/webpack",
                                            options: config,
                                        },
                                    ],
                                },
                            ],
                        },
                    },
                }) 
            );
            return config; 
        }
    }
});
