# Create-React-App + Cypress + cypress-cucumber-preprocessor
## Component and e2e tests

This repo combines a React app scaffolded with **CRA**
with **cypress-cucumber-preprocessor** being used for **both Cypress component and e2e tests**.

Note that E2E test Typescript preprocessing does not work, only JS does for now.

The first step was: `npx create-react-app . --template typescript`
The second step was simply combining two wonderful examples found
[here](https://github.com/badeball/cypress-cucumber-preprocessor/tree/master/examples/ct-react-ts), and [here](https://github.com/badeball/cypress-cucumber-preprocessor/tree/master/examples/webpack-ts) provided as part of the badeball/cypress-cucumber-preprocessor repo. Thank you, repo maintainers.

Use at your own risk.
